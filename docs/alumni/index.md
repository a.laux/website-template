# Course Alumni

This directory holds the pages for the different alumni of the course.

To add yourself as an alumni, just copy the `template.md` file and rename it to the year you took the course and your username e.g. `2023_jansim`. Then, edit the file and add your information.

To see your personal alumni page just navigate to `/alumni/<year>_<username>.html` e.g. [`/alumni/2023_jansim.html`](/alumni/2023_jansim.html).
